﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Data
{
    public class OneTimeToken
    {
        [Required]
        [Key]
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Token { get; set; }
        public bool IsVerified { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
