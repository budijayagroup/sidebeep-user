using StructureMap;
using User.Repositories;
using User.Services;

namespace User.Api {
    public static class IoC {
        private static log4net.ILog _logger = log4net.LogManager.GetLogger("Sidebeep.User");

        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });

                            x.For<IOneTimeTokenRepository>().Use<OneTimeTokenRepository>();
                            x.For<IOneTimeTokenService>().Use<OneTimeTokenService>();
                        });
            return ObjectFactory.Container;
        }
    }
}