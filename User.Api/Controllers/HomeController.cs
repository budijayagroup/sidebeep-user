﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using User.Services;

namespace User.Api.Controllers
{
    public class HomeController : Controller
    {
        public IOneTimeTokenService _oneTimeTokenService;

        public HomeController(
            IOneTimeTokenService oneTimeTokenService)
        {
            _oneTimeTokenService = oneTimeTokenService;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
