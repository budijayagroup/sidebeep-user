﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Data;
using User.ORM;

namespace User.Repositories
{
    public interface IOneTimeTokenRepository
    {
        OneTimeToken Find(int id);
        OneTimeToken FindByToken(string token);
        List<OneTimeToken> Get();
        OneTimeToken Create(OneTimeToken entity);
        bool Update(OneTimeToken entity);
        bool Delete(OneTimeToken entity);

    }

    public class OneTimeTokenRepository : IOneTimeTokenRepository
    {
        private DataContext db;

        public OneTimeTokenRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public OneTimeToken Find(int id)
        {
            var ds = db.OneTimeToken.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public OneTimeToken FindByToken(string token)
        {
            var ds = db.OneTimeToken.FirstOrDefault(x => x.Token == token);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<OneTimeToken> Get()
        {
            return db.OneTimeToken.ToList();
        }

        public OneTimeToken Create(OneTimeToken entity)
        {
            entity = db.OneTimeToken.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(OneTimeToken entity)
        {
            var ds = db.OneTimeToken.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(OneTimeToken entity)
        {
            var ds = db.OneTimeToken.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.OneTimeToken.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
