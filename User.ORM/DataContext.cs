﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Data;

namespace User.ORM
{
    public class DataContext : DbContext
    {
        public DataContext(string connection)
            : base(connection)
        {

        }

        public DbSet<OneTimeToken> OneTimeToken { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OneTimeToken>().ToTable("OneTimeToken");
        }
    }
}
